define(function(require) {
	var $ = require('jquery'),
		_ = require('lodash'),
		monster = require('monster');

	var app = {
		name: 'google-assistant',

		css: [ 'app' ],

		i18n: {
			'en-US': { customCss: false }
		},

		requests: {},

		subscribe: {},

		load: function(callback) {
			var self = this;

			self.initApp(function() {
				callback && callback(self);
			});
		},

		initApp: function(callback) {
			var self = this;

			monster.pub('auth.initApp', {
				app: self,
				callback: callback
			});
		},

		render: function(container) {
			var self = this;

			monster.ui.generateAppLayout(self, {
				menus: [
					{
						tabs: [
							{
								callback: self.renderApp
							}
						]
					}
				]
			});
		},

		renderApp: function(pArgs) {
			var self = this,
				args = pArgs || {},
				parent = args.container;

			self.userList(function(users) {
				var template = $(self.getTemplate({
					name: 'layout',
					data: {
						users: users
					}
				}));

				users.forEach(function(user) {
					self.getUserDetails(user);
				});

				self.bindEvents(template);

				parent
					.fadeOut(function() {
						$(this)
							.empty()
							.append(template)
							.fadeIn();
					});
			});
		},

		bindEvents: function(template) {
			var self = this;

			template.find('#save-ids').on('click', function() {
				$('input').each(function(i, input) {
					self.updateUser(input.value, {
						google_assistant: $(input).is(':checked')
					});
				});
			});

			template.find('.btn-ga').on('click', function(e) {
				var box = $(e.target).prev('input');
				box.click();
			});
		},

		updateUser: function(userId, updateData) {
			var self = this;
			self.callApi({
				resource: 'user.get',
				data: {
					accountId: self.accountId,
					userId: userId
				},
				success: function(data) {
					var newData = $.extend(true, {}, data.data, updateData);

					self.callApi({
						resource: 'user.update',
						data: {
							accountId: self.accountId,
							userId: userId,
							data: newData
						}
					});
				}
			});
		},

		userList: function(callback) {
			var self = this;

			self.callApi({
				resource: 'user.list',
				data: {
					accountId: self.accountId,
					filters: {
						paginate: false
					}
				},
				success: function(data) {
					callback && callback(data.data);
				}
			});
		},

		getUserDetails: function(user) {
			var self = this;

			self.callApi({
				resource: 'user.get',
				data: {
					accountId: self.accountId,
					userId: user.id
				},
				success: function(data) {
					if (data.data && data.data.google_assistant) {
						$('input[value=' + user.id + ']').prop('checked', true);
					}
				}
			});
		}
	};

	return app;
});
